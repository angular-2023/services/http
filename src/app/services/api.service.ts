import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { Menu } from '../models/menu';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getData() {
    return this.http
      .get(
        `https://api.agglo-larochelle.fr/production/opendata/api/records/1.0/search/dataset=menus_des_restaurants_scolaires`
      )
      .pipe(
        map((data: any) => data.records),
        map((records) => records.map((record: any) => record.fields)),
        map((fields) => {
          return this.buildMenus(fields);
        })
      );
  }

  buildMenus(fields: any) {
    const menus: Menu[] = [];
    fields.forEach((field: any) => {
      const menuIndex = menus.findIndex(
        (menu: any) => menu.date === field.plat_date
      );
      if (menuIndex !== -1) {
        menus[menuIndex].plats.push(field);
      } else {
        menus.push({
          date: field.plat_date,
          plats: [field],
        });
      }
    });

    return this.sortPlats(menus);
  }

  sortPlats(menus: Menu[]) {
    menus.forEach((menu) =>
      menu.plats.sort((a, b) => a.plat_ordre - b.plat_ordre)
    );

    return menus;
  }
}
