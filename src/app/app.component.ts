import { Component, OnInit } from '@angular/core';
import { Menu } from './models/menu';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  menus: Menu[] = [];
  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.getMenus();
  }

  getMenus() {
    this.apiService.getData().subscribe((response: Menu[]) => {
      this.menus = response;
    });
  }
}
